package main

import (
	"database/sql"
	"log"

	"github.com/jmoiron/sqlx"
)

type Knight struct {
	ID   int    `db:"id"`
	Name string `db:"Knight"`
	// Chief_Knights can be null, so use NullString type
	Chief_0  sql.NullString `db:"Chief_Knight"`
	Chief_1  sql.NullString `db:"Chief_Knight_1"`
	Chief_2  sql.NullString `db:"Chief_Knight_2"`
	Chief_3  sql.NullString `db:"Chief_Knight_3"`
	Chief_4  sql.NullString `db:"Chief_Knight_4"`
	Chief_5  sql.NullString `db:"Chief_Knight_5"`
	Chief_6  sql.NullString `db:"Chief_Knight_6"`
	Chief_7  sql.NullString `db:"Chief_Knight_7"`
	Chief_8  sql.NullString `db:"Chief_Knight_8"`
	Chief_9  sql.NullString `db:"Chief_Knight_9"`
	Chief_10 sql.NullString `db:"Chief_Knight_10"`
	Chief_11 sql.NullString `db:"Chief_Knight_11"`
	Chief_12 sql.NullString `db:"Chief_Knight_12"`
	Chief_13 sql.NullString `db:"Chief_Knight_13"`
}

/**
 * Returns all the knights in the database
 */
func getAllKnights(db *sqlx.DB) []map[string]interface{} {
	data := make([]map[string]interface{}, 0) // json result
	query := `SELECT * FROM knights`

	rows, err := db.Queryx(query) // query the db
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() { // loop through the result set
		var knight Knight
		err := rows.StructScan(&knight) // scan the result row's data into the struct
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, assignKnights(&knight)) // create the knight json object and append to result
	}
	return data
}

func getChiefKnights(db *sqlx.DB, id int) []map[string]interface{} {
	idParam := make(map[string]interface{}) // create named query to prevent sql injection
	idParam["id"] = id
	data := make([]map[string]interface{}, 0) // json result
	query := `SELECT * FROM knights WHERE id = :id`

	rows, err := db.NamedQuery(query, idParam) // query the db
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() { // loop through the result set
		var knight Knight
		err := rows.StructScan(&knight) // scan the result row's data into the struct
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, assignKnights(&knight)) // create the knight json object and append to result
	}
	return data
}

func getIndexedKnights(db *sqlx.DB, page int) []map[string]interface{} {
	knightsPerPage := 25 // assume we allow 25 knights each page
	startPage := page - 1
	limitParam := make(map[string]interface{}) // create named query to prevent sql injection
	limitParam["start"] = startPage * knightsPerPage
	limitParam["count"] = knightsPerPage
	data := make([]map[string]interface{}, 0) // json result
	query := `SELECT * FROM knights LIMIT :start, :count`

	rows, err := db.NamedQuery(query, limitParam) // query the db
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() { // loop through the result set
		var knight Knight
		err := rows.StructScan(&knight) // scan the result row's data into the struct
		if err != nil {
			log.Fatal(err)
		}
		data = append(data, assignKnights(&knight)) // create the knight json object and append to result
	}
	return data
}

/**
 * Assigns the chief knights, id and name from knight into knightInfo
 */
func assignKnights(knight *Knight) map[string]interface{} {
	knightInfo := make(map[string]interface{})
	chiefKnights := make([]string, 0) // array to hold all knights

	/*
	 * loop through fields of struct; TODO: use reflect package?
	 * https://stackoverflow.com/a/41737325
	 */
	for _, chiefKnight := range []interface{}{knight.Chief_0, knight.Chief_1, knight.Chief_2, knight.Chief_3, knight.Chief_4, knight.Chief_5, knight.Chief_6, knight.Chief_7, knight.Chief_8, knight.Chief_9, knight.Chief_10, knight.Chief_11, knight.Chief_12, knight.Chief_13} {
		switch chiefKnight.(type) {
		case sql.NullString:
			// type assertion to cast back to nullstring so that we can check if valid
			original, ok := chiefKnight.(sql.NullString)
			if ok {
				if original.Valid { // if non null -> add to array
					chiefKnights = append(chiefKnights, original.String)
				}
			}
		default:
			log.Println(chiefKnight)
		}
	}
	knightInfo["id"] = knight.ID
	knightInfo["name"] = knight.Name
	knightInfo["chiefKnights"] = chiefKnights

	return knightInfo
}

/**
 * Returns a JSON of chief knights for a given knight, with a limit of 25 knights.
 */
func searchKnights(db *sqlx.DB, knight string, page int) []map[string]interface{} {
	// initialize return value and base query
	chiefKnights := make([]map[string]interface{}, 0) // json result
	queryParam := make(map[string]interface{})
	queryParam["name"] = knight + "%"
	query :=
		`
	SELECT *
	FROM knights
	WHERE Knight LIKE :name
	`

	if page != 0 { // if pagination is required, customize query to return limited results
		knightsPerPage := 25 // assume we allow 25 knights each page
		startPage := page - 1
		queryParam["start"] = startPage * knightsPerPage
		queryParam["count"] = knightsPerPage
		query += `LIMIT :start, :count`
	}

	rows, err := db.NamedQuery(query, queryParam) // query the db
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() { // loop through the result set
		var knight Knight
		err := rows.StructScan(&knight) // scan the result row's data into the struct
		if err != nil {
			log.Fatal(err)
		}
		chiefKnights = append(chiefKnights, assignKnights(&knight)) // create the knight json object and append to result
	}
	return chiefKnights
}
