# legionofheroes

> A Legion of Heroes Handbook

## Installation
Clone the repo and run ```npm install``` in loh folder.

## Dependancies
ESLint - https://github.com/vuejs/eslint-plugin-vue
connect-history-api-fallback - https://github.com/bripkens/connect-history-api-fallback (DON'T USE --save OPTION)
Read the FAQ for linting issues

## Local dev
```
# hot reload at localhost:8080
npm run dev
```

## Deploy
```
# builds dist folder
npm run build

cd dist

# run the app (functionality of the app on heroku is the same as this)
npm start

# deploy to heroku
cd ..
# add, commit and push your branch to git
# make a PR to develop branch and merge your branch into develop branch
# checkout develop branch and pull merge changes

# push dist folder (minified files) to heroku
git subtree push --prefix dist heroku master

# shortcut push to heroku (check package.json for details)
npm run deploy
```
Source: https://codeburst.io/quick-n-clean-way-to-deploy-vue-webpack-apps-on-heroku-b522d3904bc8

## Issues
Dependencies in Express must be noted in package.json (see https://stackoverflow.com/questions/12127114/how-to-write-a-package-json-file-so-that-all-dependencies-are-downloaded-with-n)
Git push conflicts with Heroku (see https://stackoverflow.com/questions/45528429/i-am-getting-an-error-when-i-run-git-subtree-push-prefix-dist-heroku-master-te)