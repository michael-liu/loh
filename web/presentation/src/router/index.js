import Vue from 'vue';
import Router from 'vue-router';
import Knights from '@/components/Knights'

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home'
    },
    {
      path: '/knights',
      name: 'Knights',
      component: Knights
    }
  ],
  mode: 'history' // https://stackoverflow.com/a/34624803
});
