import Vue from 'vue';
import App from './App';
import router from './router';
/*eslint no-undef: "error"*/
/*eslint-env node*/

require('./assets/css/bulma.css') // cannot import from parent directory?
require('./assets/css/font-awesome.css') // cannot import from parent directory?
require('./assets/css/style.css') // cannot import from parent directory?

Vue.config.productionTip = true;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App),
});
