#!/bin/bash

go get -u github.com/go-sql-driver/mysql
go get -u github.com/labstack/echo
go get -u github.com/labstack/echo/middleware
go get -u github.com/jmoiron/sqlx
go get -u github.com/kardianos/govendor
go get -u gopkg.in/yaml.v2