# Building
### Local
##### Windows
1. Install Go for Windows. The setup usually sets $GOPATH to `/c/Users/YOUR_USERNAME_HERE/go`.
    - (Optional) Run `go_setup.sh` to install Go for Git Bash (you can also copy the commands directly into bash).
    - (Optional) Set the `$GOPATH` by creating a file in `~` named `.bash_profile` with these contents:
        ```bash
        export GOPATH=$HOME/go
        export PATH=$GOPATH/bin:$PATH
        ```
    - (Optional) Run `source .bash_profile` to apply the changes.
2. Create a `config.yml` configuration file in the root directory of the app:
```yaml
db_conn_string: <connection_string>
```

##### Windows Subsystem for Linux
1. Follow the instructions at: https://github.com/golang/go/wiki/Ubuntu
    ```bash
    sudo add-apt-repository ppa:longsleep/golang-backports
    sudo apt-get update
    sudo apt-get install golang-go
    ```
    This will install Go to `/usr/local/go`.
2. Run `go version` and ensure the correct version is installed.
3. Run `go env` and check the configuration of `GOPATH`; make sure it points to `/c/Users/YOUR_USERNAME_HERE/go`.
    If it doesn't, set the environment variables manually.
    ```bash
    nano ~/.bashrc # open the file in editor

    # add these 2 lines to the end of the file
    export GOPATH=/mnt/c/users/YOUR_USERNAME_HERE/go
    export PATH=$GOPATH/bin:$PATH
    ```
4. Run `source ~/.bashrc` to apply the changes.

### Production
1. Set Heroku remote: heroku git:remote -a legionofheroes-api https://devcenter.heroku.com/articles/git#for-an-existing-heroku-app
2. Branch off develop and name it master `git checkout develop && git branch -D master && git checkout -b master`
3. Run `git push heroku master`