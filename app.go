package main

import (
	// "database/sql"
	// "fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"gopkg.in/yaml.v2"
)

type EnvironmentConfig struct {
	/* A data type to hold environment variables */
	DBConnString string `yaml:"db_conn_string"`
}

// https://echo.labstack.com/cookbook/middleware#response
func ServerHeader(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		c.Response().Header().Set("Cache-Control", "max-age=604800") // 30 mins
		return next(c)
	}
}

func main() {
	e := echo.New() // create new echo instance

	// set env variables
	port := os.Getenv("PORT")
	if port == "" { // if local env, set port and don't serve static files
		log.Print("Set development PORT to 8000")
		port = "8000"
		// e.Static("/static", "web/static/static") // debugging only, comment out when deploying
		// e.Static("/index.html", "web/static/index.html")
		// e.Static("/manifest.json", "web/static/manifest.json")
		// e.Static("/", "web/static/index.html")
		e.Static("/static", "web/static/static") // debugging only, comment out when deploying
	} else { // if prod env, serve static files
		// e.Static("/", "web/static")
		// all requests for static files (js/css) will be handled by this route
		e.Static("/static", "web/static/static")
	}
	dbConnString := os.Getenv("DB_CONN_STRING")
	var config EnvironmentConfig
	if dbConnString == "" { // if local env
		// read configuration file and set dbConnString
		// https://mtyurt.net/post/go-using-environment-variables-in-configuration-files.html
		configuration, err := ioutil.ReadFile("config/config.yml")
		if err != nil {
			panic(err)
		}
		if err := yaml.Unmarshal(configuration, &config); err != nil { // decode into environment struct
			panic(err)
		}
		dbConnString = config.DBConnString
		log.Print("Set dbConnString")
	}

	// dbx connection;
	// connection string format: http://wysocki.in/golang-sqlx/, http://go-database-sql.org/accessing.html
	dbConnString = dbConnString[8:]
	userAndPassIndex := strings.Index(dbConnString, "@")
	hostAndPortIndex := strings.Index(dbConnString, "/")
	userAndPass := dbConnString[:userAndPassIndex]
	hostAndPort := dbConnString[userAndPassIndex+1 : hostAndPortIndex]
	database := dbConnString[hostAndPortIndex+1:]
	dbx, err := sqlx.Connect("mysql", userAndPass+"@("+hostAndPort+")/"+database)
	if err != nil {
		log.Print(err)
		log.Print("Error connecting to DB")
		log.Fatal("Error connecting to DB") // comment out in local environment
	}

	/**
	 * Returns a json of all knights if page is empty, returns a specific page of knights if page is given
	 */
	e.GET("/api/knights", func(c echo.Context) error {
		page := c.QueryParam("page")
		if page == "" { // if page param is not passed, return all knights
			return c.JSON(http.StatusOK, getAllKnights(dbx)) // call knights handler
		} else { // if page param is passed, return specific page of knights
			page, err := strconv.Atoi(page)
			if err != nil {
				return echo.NewHTTPError(http.StatusNotFound, "Invalid page")
			}
			return c.JSON(http.StatusOK, getIndexedKnights(dbx, page))
		}
	})

	e.GET("/api/knights/:id", func(c echo.Context) error {
		/* at /knights/:id, a json array of the provided knight and their respective chief knights is returned */
		id, err := strconv.Atoi(c.Param("id")) // get the id as a path parameter
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, "Invalid id")
		}
		return c.JSON(http.StatusOK, getChiefKnights(dbx, id)) // return knights handler
	})

	/**
	 * Searches chief knights for a given knight by name and returns 25 knights per page.
	 */
	e.GET("/api/knights/names", func(c echo.Context) error {
		// parameters sent to the endpoint
		query := c.QueryParam("match")
		page := c.QueryParam("page")
		if page == "" && query == "" { // if invalid params were passed
			return echo.NewHTTPError(http.StatusNotFound, "Invalid params")
		} else if page == "" { // if we want to get all knights
			return c.JSON(http.StatusOK, searchKnights(dbx, query, 0))
		} else {
			page, err := strconv.Atoi(page)
			if err != nil {
				return echo.NewHTTPError(http.StatusNotFound, "Invalid page")
			}
			return c.JSON(http.StatusOK, searchKnights(dbx, query, page))
		}
	})

	e.File("/manifest.json", "web/static/manifest.json") // match manifest before matching all
	// since index file is outside static directory, requests for index.html or a route after index.html are redirected
	// back to index.html (index.html is SPA, so it handles routes by itself)
	e.File("/*", "web/static/index.html")

	e.Logger.Fatal(e.Start(":" + port)) // listen on port
}
